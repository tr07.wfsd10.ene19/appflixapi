var Pelicula = require('../Model/Pelicula')

const PeliculaController = {}

PeliculaController.getMovies = (req,res) => {

    Pelicula.find(

        {}, null, null,(err,movies) => {

        if(err || !movies){ 

            res.status(404);

        }return res.send(movies).status(200);
    })
}

PeliculaController.getMoviesByGenre = (req,res) => {

    if (!req.params.genero) {  

        return res.status(404);

       }

    Pelicula.find({genre_ids : req.params.genero}, (err,movies) => {
        
        if(err) console.log(err)
        return res.send(movies)
    })
}

PeliculaController.getMovieById = (req,res) => {
    Pelicula.findById(
        req.params.id, (err,movie) => {
        if(err) console.log(err)
        return res.send(movie)
    })
}

PeliculaController.searchMoviesByTitle = (req,res) => {
    Pelicula.find( 
        {title : { $regex : req.params.title, $options: 'i'}}, 
        (err, movies) => {
        if (err) console.log(err)
        return res.send(movies)
    })
}

PeliculaController.getRelatedMovies = (req, res) =>{
    Pelicula.find({
        "genre_ids.0" : req.params.genero}
        ).where('_id').ne(req.params.id).exec(
        (error, movies)=>
        {
            if(error){
                console.log(error);
            }else{
                return res.send(movies);
        }
        })
}


module.exports = PeliculaController