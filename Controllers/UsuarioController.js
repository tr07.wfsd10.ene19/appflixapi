var Usuario = require('../Model/Usuario')

const UserController = {}

UserController.getUsers = (req, res) => {
    Usuario.find({}, (err, users) => {
        if (err) console.log(err)
        return res.send(users)
    })
}

UserController.getUserById = (req,res) => {
    if(req.params.userId) {
        Usuario.find({_id: req.params.userId}, (err, doc) => {
            if(err){
               return res.send(`Ha ocurrido un error, ${err}`)
            }
            if(doc.length > 0) return res.send(doc)
            else return res.send('No se enocntro el usuario buscado')
        })
    }
}

UserController.createUser = (req, res) => {
    Usuario.find({
        username: req.body.username
    }, (err, doc) => {
        if (err) console.log(err)
        if (doc.length > 0) {
            res.send('Ya existe un usuario con ese nombre')
        } else {
            var user = new Usuario({
                nombre: req.body.nombre,
                apellido: req.body.apellido,
                username: req.body.username,
                password: req.body.password,
                email: req.body.email
            })
            user.save(err => {
                if (err) return handleError(`No se pudo realizar el registro Error: ${err}`)
                else return res.send('Usuario resgistrado con exito')
            })
        }
    })
}

UserController.updateUser = (req, res) => {
    if (req.params.userId) {
        Usuario.find({_id: req.params.userId}, (err, doc) => {
            if (err) console.log(`Ha ocurrido un error, ${err}`)
            if (doc.length > 0) {
                doc.nombre = req.body.nombre
                doc.apellido = req.body.apellido
                doc.username = req.body.username
                doc.password = req.body.password
                doc.email = req.body.email
                doc.save(err => {
                    if (err) return handleError(`No se pudo realizar la actualizacion, Error: ${err}`)
                    else return res.send('Usuario actualizado con exito')
                })
            }
            else {
                res.send('no se ha encontrado ese usuario')
            }
        })
    }
}

UserController.deleteUser = (req, res) => {
    if (req.params.userId) {
    Usuario.deleteOne({
        _id: req.params.userId
    }, err => {
        if (err) return res.send('No se pudo eliminar el usuario')
        else return res.send('Se ha eliminado el usuario')
    })
}
}

module.exports = UserController