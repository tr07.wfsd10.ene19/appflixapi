var mongoose = require('mongoose')

var userSchema = new mongoose.Schema({
    nombre:String,
    apellido: String,
    username:String,
    password:String,
    email:String,
    favoritas:Array
})

var Usuario = mongoose.model('Usuario',userSchema)

module.exports = Usuario