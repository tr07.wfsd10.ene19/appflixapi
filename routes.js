const UserController = require('./Controllers/UsuarioController')
const PeliculaController = require('./Controllers/PeliculasController')

const generateRoutes = (app) =>  {
    //------- Rutas usuarios -------
    app.get('/usuarios', UserController.getUsers)

    app.post('/usuario', UserController.createUser)

    app.get('/usuario/:userId',UserController.getUserById)

    app.put('/usuario/:userId', UserController.updateUser)

    app.delete('/usuario/:userId', UserController.deleteUser)
    
    //------- Rutas usuarios -------


    //------- Rutas peliculas -------
    app.get('/peliculas', PeliculaController.getMovies)

    app.get('/pelicula/:id', PeliculaController.getMovieById)

    app.get('/peliculas/related/:idGenero', PeliculaController.getMoviesByGenre)

    app.get('/peliculas/related/:genero/:id', PeliculaController.getRelatedMovies)

    app.get('/peliculas/buscar/:title', PeliculaController.searchMoviesByTitle)
}
//------- Rutas peliculas -------

module.exports = generateRoutes