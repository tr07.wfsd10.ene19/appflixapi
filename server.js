const express = require ('express')
var bodyparser = require('body-parser')
var mongoose = require('mongoose')
const generateRoutes = require('./routes')

var app = express()

app.use( (req, res, next) => {

    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');

    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');

    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    // res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});


app.use(bodyparser.json())

app.use(express.static(__dirname + '/public'))

generateRoutes(app);

const PUERTO = 3000

//ruta a la BBDD
const DBURL = 'mongodb+srv://admin:upgrade123hub@cluster0-pa9tf.mongodb.net/appflix?retryWrites=true'
mongoose.connect(DBURL)

//conexion a la BBDD
var connect = mongoose.connection

connect.on('error', console.error.bind(console,'No se pudo conectar a la BBDD'))

connect.once('open',() => {
    console.log(`Conectado a la BBDD, escuchando en puerto ${PUERTO}`)
})

app.listen(PUERTO)